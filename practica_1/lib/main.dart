import 'package:flutter/material.dart';

void main() {
  runApp(EjemploConEstado());
}

/*class EjemploSinEstado extends StatelessWidget {
  const EjemploSinEstado({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    int contador = 0;

    return MaterialApp(
          home: Scaffold(
            appBar: AppBar(
              title: Text('Practica 1', style: TextStyle(color: Colors.black),),
            ),
          body: Center(
            child: Text('Valor del contador: $contador'),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.chevron_right),
            onPressed: (){
              contador++;
              print('El valor del contador es: $contador');
            },
          ),
      ),
    );
  } //
}*/

class EjemploConEstado extends StatefulWidget {
  EjemploConEstado({Key key}) : super(key: key);

  @override
  _EjemploConEstadoState createState() => _EjemploConEstadoState();
}

class _EjemploConEstadoState extends State<EjemploConEstado> {
  @override
  Widget build(BuildContext context) {
    int contador = 0;

    return MaterialApp(
          home: Scaffold(
            appBar: AppBar(
              title: Text('Practica 1', style: TextStyle(color: Colors.black),),
            ),
          body: Center(
            child: Text('Valor del contador: $contador'),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.chevron_right),
            onPressed: (){
              contador++;
              print('El valor del contador es: $contador');
              setState(() {});
            },
          ),
      ),
    );
  }
}