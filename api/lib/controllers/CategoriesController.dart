import 'package:api/helpers/configuration.dart';
import 'package:api/models/CategoriesDAO.dart';
import 'package:api/api.dart';

class CategoriesController extends ResourceController {
  
  CategoriesController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllCategories(@Bind.header('authorization') String authHeader) async {
    if (!Properties.isAutorized(authHeader)){
      return Response.forbidden();
    }

    final categoriesQuery = Query<CategoriesDAO>(context);

    final categories = await categoriesQuery.fetch();

    return Response.ok(categories);
  }

  @Operation.get('idCategory')
  Future<Response> getCategory(@Bind.path('idCategory') int idCategory) async {
    final categoriesQuery = Query<CategoriesDAO>(context)..where((x) => x.idCategory).equalTo(idCategory);
    final category = await categoriesQuery.fetchOne();

    return (category == null)? Response.notFound() : Response.ok(category);
  }

  @Operation.post()
  Future<Response> addCategory() async {
    final category = CategoriesDAO()..read(await request.body.decode(), ignore: ['idCategory']);

    final categoriesQuery = Query<CategoriesDAO>(context)..values = category;
    final addCategory = await categoriesQuery.insert();

    return Response.ok(addCategory);

  }

  @Operation.put('idCategory')
  Future<Response> updateCategory(@Bind.path('idCategory') int idCategory) async {
    final category = CategoriesDAO()..read(await request.body.decode());

    final categoriesQuery = Query<CategoriesDAO>(context)..where((x) => x.idCategory).equalTo(idCategory)..values = category;
    final updateCategory = await categoriesQuery.update();

    return Response.ok(updateCategory);
  }

  @Operation.delete('idCategory')
  Future<Response> deleteCategory(@Bind.path('idCategory') int idCategory) async {
    final categoriesQuery = Query<CategoriesDAO>(context)..where((x) => x.idCategory).equalTo(idCategory);
    final deleteCategory = await categoriesQuery.delete();

    return Response.ok(deleteCategory);
  }
  
}