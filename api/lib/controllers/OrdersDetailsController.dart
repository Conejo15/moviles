import 'package:api/models/OrdersDetailsDAO.dart';
import 'package:api/api.dart';

class OrdersDetailsController extends ResourceController {
  
  OrdersDetailsController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllOrdersDetails() async {
    final ordersDetailsQuery = Query<OrdersDetailsDAO>(context);

    final ordersDetails = await ordersDetailsQuery.fetch();

    return Response.ok(ordersDetails);
  }

  @Operation.get('idDetail')
  Future<Response> getOrderDetail(@Bind.path('idDetail') int idDetail) async {
    final orderDetailQuery = Query<OrdersDetailsDAO>(context)..where((x) => x.idDetail).equalTo(idDetail);
    final orderDetail = await orderDetailQuery.fetchOne();

    return (orderDetail == null)? Response.notFound() : Response.ok(orderDetail);
  }

  @Operation.post()
  Future<Response> addOrderDetail() async {
    final orderDetail = OrdersDetailsDAO()..read(await request.body.decode(), ignore: ['idDetail']);

    final orderDetailQuery = Query<OrdersDetailsDAO>(context)..values = orderDetail;
    final addOrderDetail = await orderDetailQuery.insert();

    return Response.ok(addOrderDetail);

  }

  @Operation.put('idDetail')
  Future<Response> updateOrderDetail(@Bind.path('idDetail') int idDetail) async {
    final orderDetail = OrdersDetailsDAO()..read(await request.body.decode());

    final orderDetailQuery = Query<OrdersDetailsDAO>(context)..where((x) => x.idDetail).equalTo(idDetail)..values = orderDetail;
    final updateOrderDetail = await orderDetailQuery.update();

    return Response.ok(updateOrderDetail);
  }

  @Operation.delete('idDetail')
  Future<Response> deleteOrderDetail(@Bind.path('idDetail') int idDetail) async {
    final orderDetailQuery = Query<OrdersDetailsDAO>(context)..where((x) => x.idDetail).equalTo(idDetail);
    final deleteOrderDetail = await orderDetailQuery.delete();

    return Response.ok(deleteOrderDetail);
  }
  
}