import 'package:jaguar_jwt/jaguar_jwt.dart';
import 'package:api/helpers/configuration.dart';
import 'package:api/api.dart';

class RestrictedController extends ResourceController {

  @Operation.get()
  Future<Response> restricted(@Bind.header("authriztion") String authHeader) async {
    if(! _isAuthorized(authHeader)) {
      return Response.forbidden();
    }

    return Response.ok("Resultado de la petición");
  }

  bool _isAuthorized(String authHeder) {
    final parts = authHeder.split(' ');
    if ( parts == null || parts.length != 2 || parts[0] != 'Bearer') {
      return false;
    }
    return _isValidToken(parts[1]);
  }

  bool _isValidToken(String token) {
    const key = Properties.jwtSecret;
    try {
      verifyJwtHS256Signature(token, key);
      return true;
    } on JwtException {
      print('Invalid token!!');
    }
    return false;
  }
  
}