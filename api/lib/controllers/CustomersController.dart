import 'package:api/models/CustomersDAO.dart';
import 'package:api/api.dart';

class CustomersController extends ResourceController {
  
  CustomersController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllCustomers() async {
    final customersQuery = Query<CustomersDAO>(context);

    final customers = await customersQuery.fetch();

    return Response.ok(customers);
  }

  @Operation.get('idCustomer')
  Future<Response> getCustomer(@Bind.path('idCustomer') int idCustomer) async {
    final customerQuery = Query<CustomersDAO>(context)..where((x) => x.idCustomer).equalTo(idCustomer);
    final customer = await customerQuery.fetchOne();

    return (customer == null)? Response.notFound() : Response.ok(customer);
  }

  @Operation.post()
  Future<Response> addCustomer() async {
    final customer = CustomersDAO()..read(await request.body.decode(), ignore: ['idCustomer']);

    final customerQuery = Query<CustomersDAO>(context)..values = customer;
    final addCustomer = await customerQuery.insert();

    return Response.ok(addCustomer);

  }

  @Operation.put('idCustomer')
  Future<Response> updateCustomer(@Bind.path('idCustomer') int idCustomer) async {
    final customer = CustomersDAO()..read(await request.body.decode());

    final customerQuery = Query<CustomersDAO>(context)..where((x) => x.idCustomer).equalTo(idCustomer)..values = customer;
    final updateCustomer = await customerQuery.update();

    return Response.ok(updateCustomer);
  }

  @Operation.delete('idCustomer')
  Future<Response> deleteCustomer(@Bind.path('idCustomer') int idCustomer) async {
    final customerQuery = Query<CustomersDAO>(context)..where((x) => x.idCustomer).equalTo(idCustomer);
    final deleteCustomer = await customerQuery.delete();

    return Response.ok(deleteCustomer);
  }
  
}