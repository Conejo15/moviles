import 'package:api/models/OrdersDAO.dart';
import 'package:api/api.dart';

class OrdersController extends ResourceController {
  
  OrdersController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllOrders() async {
    final ordersQuery = Query<OrdersDAO>(context);

    final orders = await ordersQuery.fetch();

    return Response.ok(orders);
  }

  @Operation.get('idOrder')
  Future<Response> getOrder(@Bind.path('idOrder') int idOrder) async {
    final orderQuery = Query<OrdersDAO>(context)..where((x) => x.idOrder).equalTo(idOrder);
    final order = await orderQuery.fetchOne();

    return (order == null)? Response.notFound() : Response.ok(order);
  }

  @Operation.post()
  Future<Response> addOrder() async {
    final order = OrdersDAO()..read(await request.body.decode(), ignore: ['idOrder']);

    final orderQuery = Query<OrdersDAO>(context)..values = order;
    final addOrder = await orderQuery.insert();

    return Response.ok(addOrder);

  }

  @Operation.put('idOrder')
  Future<Response> updateOrder(@Bind.path('idOrder') int idOrder) async {
    final order = OrdersDAO()..read(await request.body.decode());

    final orderQuery = Query<OrdersDAO>(context)..where((x) => x.idOrder).equalTo(idOrder)..values = order;
    final updateOrder = await orderQuery.update();

    return Response.ok(updateOrder);
  }

  @Operation.delete('idOrder')
  Future<Response> deleteOrder(@Bind.path('idOrder') int idOrder) async {
    final orderQuery = Query<OrdersDAO>(context)..where((x) => x.idOrder).equalTo(idOrder);
    final deleteOrder = await orderQuery.delete();

    return Response.ok(deleteOrder);
  }
  
}