import 'package:api/models/ProductsDAO.dart';
import 'package:api/api.dart';

class ProductsController extends ResourceController {
  
  ProductsController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllProducts() async {
    final productsQuery = Query<ProductsDAO>(context);

    final products = await productsQuery.fetch();

    return Response.ok(products);
  }

  @Operation.get('idProduct')
  Future<Response> getProduct(@Bind.path('idProduct') int idProduct) async {
    final productQuery = Query<ProductsDAO>(context)..where((x) => x.idProduct).equalTo(idProduct);
    final product = await productQuery.fetchOne();

    return (product == null)? Response.notFound() : Response.ok(product);
  }

  @Operation.post()
  Future<Response> addProduct() async {
    final product = ProductsDAO()..read(await request.body.decode(), ignore: ['idProduct']);

    final productQuery = Query<ProductsDAO>(context)..values = product;
    final addProduct = await productQuery.insert();

    return Response.ok(addProduct);

  }

  @Operation.put('idProduct')
  Future<Response> updateProduct(@Bind.path('idProduct') int idProduct) async {
    final product = ProductsDAO()..read(await request.body.decode());

    final productQuery = Query<ProductsDAO>(context)..where((x) => x.idProduct).equalTo(idProduct)..values = product;
    final updateProduct = await productQuery.update();

    return Response.ok(updateProduct);
  }

  @Operation.delete('idProduct')
  Future<Response> deleteProduct(@Bind.path('idProduct') int idProduct) async {
    final productQuery = Query<ProductsDAO>(context)..where((x) => x.idProduct).equalTo(idProduct);
    final deleteProduct = await productQuery.delete();

    return Response.ok(deleteProduct);
  }
  
}