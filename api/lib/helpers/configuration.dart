import 'package:jaguar_jwt/jaguar_jwt.dart';

class Properties {
  static const String jwtSecret = 'SADFH14F654D1GF654D1FH896FJ1YU9K4FG56HF45GN61N89YT';

  static bool isAutorized(String authHeader) {
    final parts = authHeader.split(' ');
    if (parts == null || parts.length != 2 || parts[0] !=  'Bearer') {
      return false;
    }
    return _isValidToken(parts[1]);
  }

  static bool _isValidToken(String token) {
    const key = Properties.jwtSecret;
    try {
      verifyJwtHS256Signature(token, key);
      return true;
    } on JwtException {
      print('Invalid token!!');
    }
    return false;
  }
}