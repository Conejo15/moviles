import 'package:api/controllers/CategoriesController.dart';
import 'package:api/controllers/CustomersController.dart';
import 'package:api/controllers/OrdersController.dart';
import 'package:api/controllers/OrdersDetailsController.dart';
import 'package:api/controllers/ProductsController.dart';

import 'api.dart';

/// This type initializes an application.
///
/// Override methods in this class to set up routes and initialize services like
/// database connections. See http://aqueduct.io/docs/http/channel/.
class ApiChannel extends ApplicationChannel {
  /// Initialize services in this method.
  ///
  /// Implement this method to initialize services, read values from [options]
  /// and any other initialization required before constructing [entryPoint].
  ///
  /// This method is invoked prior to [entryPoint] being accessed.
  /// Variable para la conexion a la db
  ManagedContext context;

  @override
  Future prepare() async {
    logger.onRecord.listen((rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));

    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final persistentStore = PostgreSQLPersistentStore('nest','nest','localhost', 5432, 'ventas_db');
    //flutter pub global run aqueduct:aqueduct db upgrade

    context = ManagedContext(dataModel, persistentStore);
  }

  /// Construct the request channel.
  ///
  /// Return an instance of some [Controller] that will be the initial receiver
  /// of all [Request]s.
  ///
  /// This method is invoked after [prepare].
  @override
  Controller get entryPoint {
    final router = Router();

    // Prefer to use `link` instead of `linkFunction`.
    // See: https://aqueduct.io/docs/http/request_controller/
    
    router.route("/categories[/:idCategory]").link(() => CategoriesController(context));
    router.route("/customers[/:idCustomer]").link(() => CustomersController(context));
    router.route("/orders[/:idOrder]").link(() => OrdersController(context));
    router.route("/details[/:idDetail]").link(() => OrdersDetailsController(context));
    router.route("/products[/:idProduct]").link(() => ProductsController(context));

    return router;
  }
}