import 'package:api/models/OrdersDAO.dart';
import 'package:api/api.dart';

class CustomersDAO extends ManagedObject<tblCustomers> implements tblCustomers {
  
}

class tblCustomers {
  @primaryKey
  int idCustomer;

  @Column()
  String nameCustomer;

  String addCustomer;
  
  @Column(unique: true)
  String emailCustomer;

  String phoneCustomer;

  ManagedSet<OrdersDAO> fkCustomer; //1-n

}