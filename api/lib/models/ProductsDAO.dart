import 'package:api/models/CategoriesDAO.dart';
import 'package:api/models/OrdersDetailsDAO.dart';
import 'package:api/api.dart';

class ProductsDAO extends ManagedObject<tblProducts> implements tblProducts {
  
}

class tblProducts {
  @primaryKey
  int idProduct;

  @Column(unique: true)
  String nameProduct;

  @Column()
  double price;

  @Column()
  int stock;

  @Column()
  bool discontinued;

  @Relate(#fkCategory)
  CategoriesDAO idCategory;

  ManagedSet<OrdersDetailsDAO> fkProduct;

}