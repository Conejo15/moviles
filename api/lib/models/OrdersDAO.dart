import 'package:api/models/CustomersDAO.dart';
import 'package:api/models/OrdersDetailsDAO.dart';
import 'package:api/api.dart';

class OrdersDAO extends ManagedObject<tblOrders> implements tblOrders {
  
}

class tblOrders {
  @primaryKey
  int idOrder;

  @Column(indexed: true)
  DateTime dateOrder;

  @Column(indexed: true)
  DateTime shippedDate;

  @Relate(#fkCustomer)
  CustomersDAO idCustomer;

  ManagedSet<OrdersDetailsDAO> fkOrder;
}