import 'package:api/models/ProductsDAO.dart';
import 'package:api/api.dart';

class CategoriesDAO extends ManagedObject<tblCategories> implements tblCategories {
  
}

class tblCategories {
  @primaryKey
  int idCategory;

  @Column(unique: true)
  String nameCategory;

  ManagedSet<ProductsDAO> fkCategory;//1-n
}