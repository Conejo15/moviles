import 'package:api/models/OrdersDAO.dart';
import 'package:api/models/ProductsDAO.dart';
import 'package:api/api.dart';

class OrdersDetailsDAO extends ManagedObject<tblOrdersDetails> implements tblOrdersDetails {
  
}

class tblOrdersDetails {
  @primaryKey
  int idDetail;

  double price;

  double discount;

  double quantity;

  @Relate(#fkProduct)
  ProductsDAO idProduct;

  @Relate(#fkOrder)
  OrdersDAO idOrder;

}