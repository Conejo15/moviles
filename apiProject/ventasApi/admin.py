from django.contrib import admin
from .models import Categories, Customers, Products, OrderDetails, Orders

admin.site.register( Categories)
admin.site.register( Customers )
admin.site.register( Products )
admin.site.register( OrderDetails )
admin.site.register( Orders )