from django.db import models
from datetime import datetime

class Categories( models.Model ) :
        idCategory = models.AutoField( primary_key = True)
        nameCategory = models.CharField( max_length = 50, unique = True )

        def __str__(self):
            return self.nameCategory

class Customers( models.Model ) :
        idCustomers = models.AutoField( primary_key = True)
        nameCustomers = models.CharField( max_length = 150 )
        addressCustomers = models.CharField( max_length = 200 )
        emailCustomers = models.CharField( max_length = 100, unique = True )
        phoneCustomers = models.CharField( max_length = 12 )

        def __str__(self):
            return self.nameCustomers

class Products( models.Model ) :
        idProducts = models.AutoField( primary_key = True)
        nameProducts = models.CharField( max_length = 150 )
        priceProducts = models.DecimalField( max_digits = 5, decimal_places = 2 )
        stockProducts = models.IntegerField()
        discontinuedProducts = models.BooleanField()
        idCategory = models.IntegerField()

        def __str__(self):
            return self.nameProducts
        
class OrderDetails( models.Model ) :
        idOrderDetails = models.AutoField( primary_key = True)
        priceOrderDetails = models.DecimalField( max_digits = 5, decimal_places = 2 )
        discountOrderDetails = models.DecimalField( max_digits = 5, decimal_places = 2 )
        quantityOrderDetails = models.DecimalField( max_digits = 5, decimal_places = 2 )
        idProductOrderDetails = models.IntegerField()
        idOrderOrderDetails = models.IntegerField()

        def __str__(self):
            return self.idProductOrderDetails
        
class Orders( models.Model ) :
        idOrders = models.AutoField( primary_key = True)
        dateOrders =  models.DateTimeField( default = datetime.now, blank=True)
        shippedDateOrders = models.DateTimeField( default = datetime.now, blank=True)
        idCustomerOrders = models.IntegerField()

        def __str__(self):
            return self.idOrders