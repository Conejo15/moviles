from django.urls import path
from ventasApi import views

urlpatterns = [
    path('categories/', views.crud_categories ),
    path('category/<int:id>', views.crud_categories_id ),
    path('customers/', views.crud_customers ),
    path('customer/<int:id>', views.crud_customers_id ),
    path('products/', views.crud_products ),
    path('product/<int:id>', views.crud_products_id ),
    path('orderdetails/', views.crud_orderdetails ),
    path('orderdetail/<int:id>', views.crud_orderdetails_id ),
    path('orders/', views.crud_orders ),
    path('order/<int:id>', views.crud_orders_id ),
]
