from rest_framework import serializers
from .models import Categories, Customers, Products, OrderDetails, Orders

class CategoriesSerializer( serializers.ModelSerializer ):
    class Meta:
        model = Categories
        fields = (
            'idCategory',
            'nameCategory',
        )

class CustomersSerializer( serializers.ModelSerializer ):
    class Meta:
        model = Customers
        fields = (
            'idCustomers',
            'nameCustomers',
            'addressCustomers',
            'emailCustomers',
            'phoneCustomers',
        )

class ProductsSerializer( serializers.ModelSerializer ):
    class Meta:
        model = Products
        fields = (
            'idProducts',
            'nameProducts',
            'priceProducts',
            'stockProducts',
            'discontinuedProducts',
            'idCategory',
        )

class OrderDetailsSerializer( serializers.ModelSerializer ):
    class Meta:
        model = OrderDetails
        fields = (
            'idOrderDetails',
            'priceOrderDetails',
            'discountOrderDetails',
            'quantityOrderDetails',
            'idProductOrderDetails',
            'idOrderOrderDetails',
        )

class OrdersSerializer( serializers.ModelSerializer ):
    class Meta:
        model = Orders
        fields = (
            'idOrders',
            'dateOrders',
            'shippedDateOrders',
            'idCustomerOrders',
        )
