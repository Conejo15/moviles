# Generated by Django 3.1.3 on 2020-11-18 03:50

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('idCategory', models.AutoField(primary_key=True, serialize=False)),
                ('nameCategory', models.CharField(max_length=50, unique=True)),
                ('stock', models.IntegerField()),
                ('decimal', models.DecimalField(decimal_places=2, max_digits=5)),
                ('date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
    ]
