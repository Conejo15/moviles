from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Categories, Customers, Products, OrderDetails, Orders
from .serializers import CategoriesSerializer, CustomersSerializer, ProductsSerializer, OrderDetailsSerializer, OrdersSerializer

@api_view(['GET','PUT','DELETE'])
def crud_categories_id( request, id ):
    try:
        data = Categories.objects.get( idCategory = id )
    except Categories.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response ( CategoriesSerializer(data).data )

    if request.method == 'PUT' :
        serializer = CategoriesSerializer( data, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_200_OK )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

    if request.method == 'DELETE' :
        data.delete()
        return Response( status = status.HTTP_200_OK )

@api_view(['GET','POST'])
def crud_categories( request ):
    try:
        data = Categories.objects.all()
    except Categories.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response( CategoriesSerializer( Categories.objects.all(), many = True ).data )

    if request.method == 'POST' :
        serializer = CategoriesSerializer( data = request.data )
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_201_CREATED )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

@api_view(['GET','PUT','DELETE'])
def crud_customers_id( request, id ):
    try:
        data = Customers.objects.get( idCustomers = id )
    except Customers.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response ( CustomersSerializer(data).data )

    if request.method == 'PUT' :
        serializer = CustomersSerializer( data, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_200_OK )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

    if request.method == 'DELETE' :
        data.delete()
        return Response( status = status.HTTP_200_OK )

@api_view(['GET','POST'])
def crud_customers( request):
    try:
        data = Customers.objects.all()
    except Customers.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response( CustomersSerializer( Customers.objects.all(), many = True ).data )

    if request.method == 'POST' :
        serializer = CustomersSerializer( data = request.data )
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_201_CREATED )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

@api_view(['GET','PUT','DELETE'])
def crud_products_id( request, id ):
    try:
        data = Products.objects.get( idProducts = id )
    except Products.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response ( ProductsSerializer(data).data )

    if request.method == 'PUT' :
        serializer = ProductsSerializer( data, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_200_OK )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

    if request.method == 'DELETE' :
        data.delete()
        return Response( status = status.HTTP_200_OK )
        
@api_view(['GET','POST'])
def crud_products( request):
    try:
        data = Products.objects.all()
    except Products.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response( ProductsSerializer( Products.objects.all(), many = True ).data )

    if request.method == 'POST' :
        serializer = ProductsSerializer( data = request.data )
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_201_CREATED )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

@api_view(['GET','PUT','DELETE'])
def crud_orderdetails_id( request, id ):
    try:
        data = OrderDetails.objects.get( idOrderDetails = id )
    except OrderDetails.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response ( OrderDetailsSerializer(data).data )

    if request.method == 'PUT' :
        serializer = OrderDetailsSerializer( data, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_200_OK )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

    if request.method == 'DELETE' :
        data.delete()
        return Response( status = status.HTTP_200_OK )

@api_view(['GET','POST'])
def crud_orderdetails( request):
    try:
        data = OrderDetails.objects.all()
    except OrderDetails.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response( OrderDetailsSerializer( OrderDetails.objects.all(), many = True ).data )

    if request.method == 'POST' :
        serializer = OrderDetailsSerializer( data = request.data )
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_201_CREATED )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

@api_view(['GET','PUT','DELETE'])
def crud_orders_id( request, id ):
    try:
        data = Orders.objects.get( idOrders = id )
    except Orders.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response ( OrdersSerializer(data).data )

    if request.method == 'PUT' :
        serializer = OrdersSerializer( data, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_200_OK )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )

    if request.method == 'DELETE' :
        data.delete()
        return Response( status = status.HTTP_200_OK )

@api_view(['GET','POST'])
def crud_orders( request):
    try:
        data = Orders.objects.all()
    except Orders.DoesNotExist:
        return Response( status = status.HTTP_404_NOT_FOUND )

    if request.method == 'GET' :
        return Response( OrdersSerializer( Orders.objects.all(), many = True ).data )

    if request.method == 'POST' :
        serializer = OrdersSerializer( data = request.data )
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status = status.HTTP_201_CREATED )
        return Response( serializer.errors, status = status.HTTP_400_BAD_REQUEST )