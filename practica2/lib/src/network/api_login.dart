import 'package:practica2/src/models/userdao.dart';
import 'package:practica2/src/network/coneccion.dart';
import 'dart:convert';

class ApiLogin extends ConeccionApi {
  
  Future< UserDAO >  validateUser(UserDAO objUser) async
  {
    return null; //Error de login api
    String response = await this.post("/auth/login", objUser);
    if(response != null)
    {
      final jsonResponse = json.decode(response);
      UserDAO user = new UserDAO.fromJSON(jsonResponse);
      return user;
    }else
      return null;
  }
  
  Future < bool >  checkUser(String token) async
  {
    String response = await this.get("/auth/userdata", "");
    return response != null;
  }
}