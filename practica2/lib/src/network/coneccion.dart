import 'dart:io';
import "package:http/http.dart" show Client;
import 'package:practica2/src/assets/preferences.dart';

class ConeccionApi {
  String SERVER = "";
  Client http = Client();
  Preferences pr = Preferences();

  Future post(url, data) async
  {
    String token = await pr.getString("token");
    print(data.toJSON());
    final response = await http.post(
      SERVER+url,
      headers: {HttpHeaders.authorizationHeader: "Bearer ${token}"},
      body: data.toJSON()
    );
    if( response.statusCode == 200 ) return response.body;
    else return null;
  }

  Future get(url, params) async
  {
    String token = await pr.getString("token");

    final response = await http.get(
      SERVER+url+params,
      headers: {HttpHeaders.authorizationHeader: "Bearer ${token}"},
    );
    if( response.statusCode == 200 ) return response.body;
    else return null;
  }
}