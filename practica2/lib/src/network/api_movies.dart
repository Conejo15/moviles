import 'dart:convert';

import 'package:practica2/src/models/credit.dart';
import 'package:practica2/src/models/trending.dart';
import 'package:practica2/src/models/video.dart';
import 'package:practica2/src/network/coneccion.dart';

class ApiMovies extends ConeccionApi {
  final String URL_TRENDING = "https://api.themoviedb.org/3/movie/";

  Future<List<Result>> getTrending () async  {
    final response = await this.get(URL_TRENDING, "popular?api_key=d799bfe91d852989481196cfe2fcc2d5&language=es-LA&page=1");
    if( response != null ){
      var movies = jsonDecode( response )['results'] as List;
      List<Result> listMovies = movies.map((movie) => Result.fromJSON(movie)).toList();
      return listMovies;
    }
    return null;
  }

  Future<String> getVideo (int idmovie) async  {
    final response = await this.get(URL_TRENDING, "${idmovie}/videos?api_key=d799bfe91d852989481196cfe2fcc2d5&language=es-LA");
    if( response == null ) return null;
    var movies = jsonDecode( response )['results'] as List;
    List<Video> listVideos = movies.map((movie) => Video.fromJSON(movie)).toList();
    return  listVideos.length == 0 ? null : listVideos[0].key ;
  }

  Future<List<Credit>> getCreditos (int idmovie) async  {
    final response = await this.get(URL_TRENDING, "${idmovie}/credits?api_key=d799bfe91d852989481196cfe2fcc2d5&language=es-LA");
    if( response == null ) return null;
    var movies = jsonDecode( response )['cast'] as List;
    List<Credit> listCredits = movies.map((movie) => Credit.fromJSON(movie)).toList();
    return  listCredits ;
  }
}