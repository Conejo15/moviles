import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:practica2/src/models/trending.dart';
import 'package:practica2/src/models/userdao.dart';
import 'package:sqflite/sqflite.dart';

class DataBaseHelper {
  
  static final _nombreBD = "PATM2020";
  static final _versionBD = 1;

  static Database _database;
  Future<Database> get database async{
    if( _database != null ) return _database;

    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async{
    Directory carpeta = await getApplicationDocumentsDirectory();
    String rutaBD = join(carpeta.path,_nombreBD);
    return await openDatabase(
      rutaBD,
      version: _versionBD,
      onCreate: _crearTablas
    );
  }

  _crearTablas(Database db, int version) async{
    await db.execute("CREATE TABLE tbl_perfil( id INTEGER PRIMARY KEY, nomUser varchar(25), apepUser varchar(25), apemUser varchar(25), telUser char(10), mailUser varchar(30), foto varchar(200), usuario varchar(30), password varchar(30), curp varchar(30))");
    await db.execute("CREATE TABLE tbl_favorites( idfavorite INTEGER PRIMARY KEY,  backdrop_path varchar(100), title varchar(100), id integer, overview varchar(100), poster_path varchar(100), original_title varchar(100), release_date varchar(100), popularity decimal(52), vote_average decimal(52), vote_count integer(11), adult boolean, video boolean)");
  }

  Future<int> insertar(Map<String, dynamic> row, String tabla) async{
    var dbClient = await database;
    return await dbClient.insert(tabla, row);
  }

  Future<int> actualizar(Map<String, dynamic> row, String tabla) async{
    var dbClient = await database;
    return await dbClient.update(tabla, row, where: 'id = ?', whereArgs: [row['id']] );
  }

  Future<int> eliminar(int id, String tabla) async{
    var dbClient = await database;
    return await dbClient.delete(tabla, where: 'id = ?', whereArgs: [id] );
  }

  Future<UserDAO> getUsuario(String mailUser) async {
    var dbClient = await database;
    var result = await dbClient.query('tbl_perfil', where: 'mailUser = ?', whereArgs: [mailUser] );
    var lista = (result).map((item) => UserDAO.fromJSON(item)).toList();
    return lista.length>0 ? lista[0] : null;
  }

  Future<List<Result>> getMovies() async {
    var dbClient = await database;
    var result = await dbClient.query('tbl_favorites' );
    var lista = (result).map((item) => Result.fromJSON(item)).toList();
    return lista;
  }

  Future<bool> inFavorite(int id) async {
    var dbClient = await database;
    var result = await dbClient.query('tbl_favorites', where: 'id = ?', whereArgs: [id] );
    var lista = (result).map((item) => UserDAO.fromJSON(item)).toList();
    print(lista.length>0 ? "Si esta en favoritos" : "No lo está");
    return lista.length > 0 ? true : false ;
  }

  Future<int> eliminarTodo(String tabla) async{
    var dbClient = await database;
    return await dbClient.delete(tabla);
  }
}