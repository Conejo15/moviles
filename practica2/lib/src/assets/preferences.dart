import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
    Future < bool > setString(String key, String value) async {
      final SharedPreferences pre = await SharedPreferences.getInstance();
      return pre.setString(key, value);
    }

    Future < String > getString(String key) async {
      final SharedPreferences pre = await SharedPreferences.getInstance();
      String value = pre.getString(key) ?? null;
      return value;
    }

    Future < bool > delete() async {
      final SharedPreferences pre = await SharedPreferences.getInstance();
      return pre.clear();
    }
}
