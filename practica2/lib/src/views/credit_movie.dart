import 'package:flutter/material.dart';
import 'package:practica2/src/models/credit.dart';

class CreditMovie extends StatelessWidget{
  const CreditMovie({
    Key key,
    @required this.credit
  }):super (key:key);
  final Credit credit;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric( horizontal: 10.0, vertical: 10.0 ),
      child: new Column(
        children: <Widget>[
          Container(
            child: ClipRRect (
              borderRadius: BorderRadius.circular( 100.0 ),
              child: Stack(
                alignment: Alignment.bottomCenter,
                children:<Widget> [
                  Container(
                    width: 80,
                    child: FadeInImage(
                      placeholder: AssetImage('assets/fondo1.png'),
                      image: NetworkImage('https://image.tmdb.org/t/p/w500/${credit.profile_path}'),
                      fadeInDuration: Duration(milliseconds: 100),
                    ),
                  ),
                ],
              )
            )
          ),
          Text(
            credit.name,
            style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
          ),
        ]
      )
    );
  }

}