import 'package:flutter/material.dart';
import 'package:practica2/src/database/database_helper.dart';
import 'package:practica2/src/models/credit.dart';
import 'package:practica2/src/models/trending.dart';
import 'package:practica2/src/network/api_movies.dart';
import 'package:practica2/src/screen/detailMovie.dart';

class CardTrending extends StatelessWidget{
  const CardTrending({
    Key key,
    @required this.trending
  }):super (key:key);
  final Result trending;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric( horizontal: 10.0, vertical: 5.0 ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular( 15.0 ),
        boxShadow:[
          BoxShadow(
            color: Colors.black12,
            offset: Offset(0.0, 5.0),
            blurRadius: 1.0
          )
        ] 
      ),
      child: ClipRRect (
        borderRadius: BorderRadius.circular( 10.0 ),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children:<Widget> [
            Container(
              width: MediaQuery.of(context).size.width,
              child: FadeInImage(
                placeholder: AssetImage('assets/fondo1.png'),
                image: NetworkImage('https://image.tmdb.org/t/p/w500/${trending.backdropPath}'),
                fadeInDuration: Duration(milliseconds: 100),
              ),
            ),
            Opacity(
              opacity: .5,
              child: Container(
                height: 55.0,
                color: Colors.black, 
                padding: EdgeInsets.all( 10.0 ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget> [
                    Text( 
                      trending.title, 
                      style: TextStyle( 
                        color: Colors.white, 
                      ),
                    ),
                    RaisedButton(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10.0
                      ),
                      child: Icon(
                        Icons.chevron_right
                      ),
                      onPressed: () async{
                        DataBaseHelper _database = DataBaseHelper();
                        ApiMovies api = ApiMovies();

                        bool inFavorite = await _database.inFavorite(trending.id);
                        String key = await api.getVideo(trending.id);

                        Navigator.pop(context);
                        Navigator.push(context,  MaterialPageRoute (
                          builder: (context) => DetailMovie(trending,inFavorite, key)
                        ));
                      },
                    )
                  ],
                )
              )
            )
          ],
        )
      )
    );
  }
}