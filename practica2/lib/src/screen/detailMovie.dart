import 'package:flutter/material.dart';
import 'package:practica2/src/database/database_helper.dart';
import 'package:practica2/src/models/credit.dart';
import 'package:practica2/src/models/trending.dart';
import 'package:practica2/src/network/api_movies.dart';
import 'package:practica2/src/views/credit_movie.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class DetailMovie extends StatefulWidget {
  final Result movie;
  final bool inFavorites;
  final String video;

  const DetailMovie(this.movie, this.inFavorites, this.video, {Key key}) : super(key: key);

  @override
  _DetailMovieState createState() => _DetailMovieState();
}

class _DetailMovieState extends State<DetailMovie> {

  DataBaseHelper _database;
  Result movie;
  bool inFavorite;
  String keyVideo="";

  ApiMovies apiDetailMovie;
  @override
  void initState() {
    super.initState();
    _database = DataBaseHelper();
    apiDetailMovie = ApiMovies();

    movie = widget.movie;
    inFavorite = widget.inFavorites;
    keyVideo = widget.video;
  }

  @override
  Widget build(BuildContext context) {
    YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: keyVideo,
      flags: YoutubePlayerFlags(
          autoPlay: false,
          mute: false,
          disableDragSeek: false,
          loop: false,
          isLive: false,
          forceHD: false,
      ),
    );
    
    return Scaffold(
      appBar: AppBar(
          title: Text('Movie : ${movie.title}'),
          actions: <Widget>[
            IconButton(
              icon: new Icon( inFavorite ? Icons.favorite : Icons.favorite_border_outlined ),
              onPressed: () async {

                Result result = Result(
                  backdropPath : movie.backdropPath,
                  title : movie.title,
                  id : movie.id,
                  overview : movie.overview,
                  posterPath : "",
                  popularity : 0.0,
                  voteCount : 0,
                  video : false,
                  adult : false,
                  originalTitle : "",
                  voteAverage : 0.0,
                  releaseDate : ""
                );

                if ( inFavorite )
                {
                  await _database.eliminar( movie.id , "tbl_favorites")
                    .then((data) { print(data); });
                }else {
                  await _database.insertar(result.toJSON(), "tbl_favorites")
                    .then((data) { print(data); });
                }
                
                inFavorite = !inFavorite;
              },
            ),
          ],
        ),
      body: FutureBuilder(
        future: apiDetailMovie.getCreditos(movie.id),
        builder: (BuildContext context, AsyncSnapshot<List<Credit>> snapshot) {
          if( snapshot.hasError ) {
            return Center(
              child: Text("Error")
            );
          }else if( snapshot.connectionState == ConnectionState.done) {
            return new Stack(
              children: <Widget>[
                Expanded(
                  child: _listDetailMovie(snapshot.data)
                ),
                Card(
                  color: Colors.white70,
                  margin: EdgeInsets.only( top : 150),
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child:ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        YoutubePlayer(
                            controller: _controller,
                            liveUIColor: Colors.amber,
                        ),
                        SizedBox(height: 20),
                        Container(
                          child: FadeInImage(
                            placeholder: AssetImage('assets/fondo1.png'),
                            image: NetworkImage('https://image.tmdb.org/t/p/w500/${movie.backdropPath}'),
                            fadeInDuration: Duration(milliseconds: 100),
                          )
                        ),
                        SizedBox(height: 20),
                        Card(
                          clipBehavior: Clip.antiAlias,
                          child: Column(
                            children: [
                              ListTile(
                                title: const Text('Descripción: '),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Text(
                                  '${movie.overview}',
                                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  )
                ),
              ]
            );
          }else {
            return Center(
              child: CircularProgressIndicator()
            );
          }
        },
      )
    );
  }

  Widget _listDetailMovie(List<Credit>credits){
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemBuilder: (context,index){
          Credit credit =credits[index];
          return CreditMovie(credit : credit);
      },
      itemCount: credits.length
    );
  }
}