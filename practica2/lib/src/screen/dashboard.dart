import 'dart:io';

import 'package:flutter/material.dart';
import 'package:practica2/src/assets/configuration.dart';
import 'package:practica2/src/assets/preferences.dart';
import 'package:practica2/src/database/database_helper.dart';
import 'package:practica2/src/models/userdao.dart';
import 'package:practica2/src/screen/profile.dart';

class Dashboard extends StatefulWidget {
  final String email;
  const Dashboard(this.email, {Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  String email;
  Preferences pr = Preferences();
  DataBaseHelper _database = DataBaseHelper();

  @override
  void initState(){
    super.initState();
    email = widget.email;
    _database = DataBaseHelper();
  }

  @override
  Widget build(BuildContext context) {

  Future<UserDAO> _objUser = _database.getUsuario(email ?? "");

    return Container(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Configuration.colorApp,
          title: Text('Peliculas')
        ),
        drawer: Drawer(
          child: FutureBuilder(
            future: _objUser,
            builder: (BuildContext context, AsyncSnapshot<UserDAO> snapshot) {
              return 
              ListView(
                children: <Widget>[
                  UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                      color: Colors.blueAccent
                    ),
                    currentAccountPicture: snapshot.data == null
                    ? CircleAvatar(
                      backgroundImage: NetworkImage("http://www.abo-sc.org.br/wp-content/uploads/2017/06/img-perfil-masc2.png"),
                    )
                    :( snapshot.data.foto == ""
                      ? CircleAvatar(
                        backgroundImage: NetworkImage("http://www.abo-sc.org.br/wp-content/uploads/2017/06/img-perfil-masc2.png"),
                      )
                      :ClipOval(
                        child: Image.file(File(snapshot.data.foto)),
                      )
                    ),
                    accountName: Text( snapshot.data == null ? "Nombre" : snapshot.data.nomUser+" "+snapshot.data.apepUser+" "+snapshot.data.apemUser),
                    accountEmail: Text( snapshot.data == null ? email : snapshot.data.mailUser),
                    onDetailsPressed: () {},
                  ),
                  ListTile(
                    leading: Icon(Icons.trending_up, color: Configuration.colorItems),
                    title: Text("Trending"),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.pushNamed(context, "/trending");
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.search, color: Configuration.colorItems),
                    title: Text("Search"),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.pushNamed(context, "/search");
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.account_balance, color: Configuration.colorItems),
                    title: Text("Profile"),
                    onTap: () async {
                      Navigator.pop(context);
                      Navigator.push(context,  MaterialPageRoute (
                        builder: (context) => Profile(snapshot.data, email )
                      ));
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.favorite, color: Configuration.colorItems),
                    title: Text("Favorites"),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.pushNamed(context, "/favorites");
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.exit_to_app, color: Configuration.colorItems),
                    title: Text("sing out"),
                    onTap: () async{

                      Preferences pr = Preferences();
                      String nombre =await pr.getString("usuario");

                      await pr.delete();

                      Navigator.pop(context);
                      Navigator.pushNamed(context, "/login");
                      
                      showDialog(
                        context: context,
                        builder: (BuildContext context){
                          return  AlertDialog(
                            title: Text('Cerrar sesión'),
                            content: Text('Adios ${nombre}'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text('close')
                                )
                            ],
                          );
                        } 
                      );
                    },
                  )
                ]
              );
            }
          ),
        ),
      ),
    );
  }
}