import 'package:flutter/material.dart';
import 'package:practica2/src/screen/dashboard.dart';
import 'package:splashscreen/splashscreen.dart';

class Splashscreen extends StatefulWidget {
  Splashscreen({Key key}) : super(key: key);

  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 10,
      navigateAfterSeconds: Dashboard(""),
      title: Text('Bienvenido :)'),
      image: new Image.network('https://i.pinimg.com/originals/67/54/78/675478c7dcc17f90ffa729387685615a.jpg'),
      gradientBackground: LinearGradient(
        colors: [ Colors.white, Colors.blueGrey],
        begin: Alignment.center,
        end: Alignment.bottomCenter
      ),
      loaderColor: Colors.red,
    );
  }
}
