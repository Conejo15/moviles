import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:practica2/src/assets/preferences.dart';
import 'package:practica2/src/database/database_helper.dart';
import 'package:practica2/src/models/userdao.dart';


class Profile extends StatefulWidget {
  final UserDAO data;
  final String email;
  Profile(this.data, this.email, {Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  TextEditingController txtNombre = TextEditingController();
  TextEditingController txtApePat = TextEditingController();
  TextEditingController txtApeMat = TextEditingController();
  TextEditingController txtCurp   = TextEditingController();
  TextEditingController txtEmail   = TextEditingController();

  final picker = ImagePicker();
  String imagePath = "" ;
  DataBaseHelper _database;
  Preferences pr = Preferences();

  @override
  void initState(){
    super.initState();
    txtEmail.text = widget.email;

    if ( widget.data != null )
    {
      txtNombre.text = widget.data.nomUser;
      txtApePat.text = widget.data.apemUser;
      txtApeMat.text = widget.data.apepUser;
      txtCurp.text = widget.data.curp;
      imagePath = widget.data.foto;
    }

    _database = DataBaseHelper();
  }

  @override
  Widget build(BuildContext context) {

    Preferences pr = Preferences();

    final imgFinal = imagePath == "" 
    ? CircleAvatar(
        backgroundImage: NetworkImage("http://www.abo-sc.org.br/wp-content/uploads/2017/06/img-perfil-masc2.png"),
      )
    : ClipOval(
      child: Image.file(
                File(imagePath),
                fit: BoxFit.cover,
              ),
    );

    final guardarBtn = RaisedButton(
      color: Colors.blue,
      child: Icon(Icons.save, color: Colors.white),
      onPressed: () async {
        print(widget.email);
        final _objUser = await _database.getUsuario(widget.email);
        print(_objUser);
        UserDAO user = UserDAO(
          nomUser: txtNombre.text,
          apepUser: txtApePat.text,
          apemUser: txtApeMat.text,
          curp: txtCurp.text,
          foto: imagePath == "" ? "http://www.abo-sc.org.br/wp-content/uploads/2017/06/img-perfil-masc2.png" : imagePath,
          mailUser: widget.email,
          telUser: _objUser == null ? "" : _objUser.telUser,
          password: _objUser == null ? "" : _objUser.password,
          usuario: _objUser == null ? "" : _objUser.usuario
        );

        if( _objUser == null) {
          print("Insertar");
          await _database.insertar(user.toJSON(), "tbl_perfil")
            .then((data) => { print(data) });
        } else {
          print("Actualizar");
          user.id = _objUser.id;
          await _database.actualizar(user.toJSON(), "tbl_perfil")
            .then((data) => { print(data) });
        }
        
        Navigator.pop(context);
        Navigator.pushNamed(context, "/dashboard");
      },
    );
    
    final camara = FloatingActionButton(
      child: Icon(Icons.camera_alt, color: Colors.white),
      onPressed: () async {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        imagePath = pickedFile != null ? pickedFile.path : "" ;
        setState(() {});
      },
    ); 

    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/fondo1.png'),
              fit: BoxFit.fitHeight
            )
          )
        ),
        Card(
          color: Colors.white70,
          margin: EdgeInsets.all(30.0),
          elevation: 8.0,
          child: Padding(
            padding: EdgeInsets.all(10),
            child:ListView(
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  height: 200.0,
                  width: 200.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: imgFinal
                ),
                camara,
                SizedBox(height: 20),
                Card(
                  elevation: 6.0,
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: TextField(
                      controller: txtEmail,
                      keyboardType: TextInputType.emailAddress,
                      enabled: false,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Correo',
                      ),
                    )
                  ) 
                ),
                SizedBox(height: 20),
                Card(
                  elevation: 6.0,
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: TextField(
                      controller: txtNombre,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Nombre del usuario',
                      ),
                    )
                  ) 
                ),
                SizedBox(height: 20),
                Card(
                  elevation: 6.0,
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: TextField(
                      controller: txtApePat,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Apellido paterno',
                      ),
                    )
                  ) 
                ),
                SizedBox(height: 20),
                Card(
                  elevation: 6.0,
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: TextField(
                      controller: txtApeMat,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Apellido materno',
                      ),
                    )
                  ) 
                ),
                SizedBox(height: 20),
                Card(
                  elevation: 6.0,
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: TextField(
                      controller: txtCurp,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Curp del usuario',
                      ),
                    )
                  ) 
                ),
                guardarBtn,
              ],
            )
          )
        ),
      ]
    );
  }
}