import 'package:flutter/material.dart';
import 'package:practica2/src/database/database_helper.dart';
import 'package:practica2/src/models/trending.dart';
import 'package:practica2/src/views/card_trending.dart';

class Favorites extends StatefulWidget {
  const Favorites({Key key}) : super(key: key);
  
  @override
  _FavoritesState createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites>
{
  DataBaseHelper _database;

  @override
  void initState() {
    super.initState();
    _database = DataBaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("")
      ),
      body: FutureBuilder(
        future: _database.getMovies(),
        builder: (BuildContext context, AsyncSnapshot<List<Result>> snapshot) {
          if( snapshot.hasError ) {
            return Center(
              child: Text("Error")
            );
          }else if( snapshot.connectionState == ConnectionState.done) {
            return  _listFavorites(snapshot.data);
          }else {
            return Center(
              child: CircularProgressIndicator()
            );
          }
        },
      )
    );
  }

  Widget _listFavorites(List<Result>movies){
    return ListView.builder(
      itemBuilder: (context,index){
          Result favorites=movies[index];
          return CardTrending(trending: favorites);
      },
      itemCount: movies.length
    );
  }
}