import 'package:flutter/material.dart';
import 'package:practica2/src/assets/preferences.dart';
import 'package:practica2/src/models/userdao.dart';
import 'package:practica2/src/network/api_login.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:practica2/src/screen/dashboard.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  Preferences pr = Preferences();
  ApiLogin httpLogin = ApiLogin();
  
  bool isValidating = false;        // Variable para controlar la visualizacion del indicador de progreso

 @override
  Widget build(BuildContext context){

    TextEditingController txtUser = TextEditingController(); 
    TextEditingController txtPass = TextEditingController(); 

    final logo = CircleAvatar(
      child: Image.network("http://itcelaya.edu.mx/admision/img/itcelaya2.png")
    );

    final txtEmail = TextFormField(
      controller: txtUser,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: "Introduce el email",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 4)
      ),
    );

    final txtPwd = TextFormField(
      controller: txtPass,
      obscureText: true,
      obscuringCharacter: "*",
      keyboardType:  TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Introduce el password',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 4)
      )
    );

    final sesion = Checkbox(
      value: timeDilation == 1.0,
      activeColor: Color(0xFF6200EE),
      onChanged: (bool value) async { 
        setState(() {
          timeDilation = value ? 1.0 : 2.0;
        });
        await pr.setString("guardar", value.toString());
       },
    );

    final chk = Row(
      children: [
        sesion,
        Text(
          'Mantener la sesión activada',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.black),
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    );

    final loginButton = RaisedButton( 
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)
      ),
      child: Text('Valida Usuario', style: TextStyle(color: Colors.white)),
      color: Colors.lightBlue,
      onPressed: () async {
        
        setState(() { isValidating = true; });

        UserDAO objUser = UserDAO( usuario: txtUser.text , password: txtPass.text);
        final data = await httpLogin.validateUser(objUser);
        // if( data != null) {

          if(sesion.value)
          {
            await pr.setString("token", "data.accessToken");
            await pr.setString("email", txtUser.text);
          }

          Navigator.pop(context);
          Navigator.push(context,  MaterialPageRoute (
                        builder: (context) => Dashboard( txtUser.text )
                      ));

        // } else
        showDialog(
          context: context,
          builder: (BuildContext context){
            return  AlertDialog(
              title: Text('Error'),
              content: Text('Credenciales incorrectas'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('close')
                  )
              ],
            );
          } 
        );
      }
    );

    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/fondo1.png'),
              fit: BoxFit.fitHeight
            )
          )
        ),
        Card(
          color: Colors.white70,
          margin: EdgeInsets.all(30.0),
          elevation: 8.0,
          child: Padding(
            padding: EdgeInsets.all(10),
            child:ListView(
              shrinkWrap: true,
              children: <Widget>[
                txtEmail,
                SizedBox(height: 20,),
                txtPwd,
                SizedBox(height:10,),
                chk,
                SizedBox(height:0,),
                loginButton,
              ],
            )
          )
        ),
        Positioned(
          child: logo,
          top: 50,
          left: 200,
        ),
        Positioned(
          top: 300,
          left: 200,
          child: isValidating ? CircularProgressIndicator() : Container()
        )
      ]
    );
  }
}


