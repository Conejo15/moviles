
class Trending {
  int page;
  int totalResults;
  int totalPages;
  List<Result> results;

  Trending({
    this.page,
    this.totalResults,
    this.totalPages,
    this.results,
  });
}

class Result {
  double popularity;
  int voteCount;
  bool video;
  String posterPath;
  int id;
  bool adult;
  String backdropPath;
  String originalTitle;
  String title;
  double voteAverage;
  String overview;
  String releaseDate;

  factory Result.fromJSON(Map<String, dynamic> map){
    return Result(
      id: map['id'],
      backdropPath: map['backdrop_path'],
      title: map['title'],
      overview: map['overview'],
    );
  }

  Map<String,dynamic> toJSON(){
    return {
      "popularity"  : popularity,
      "vote_count"  : voteCount,
      "video" : video,
      "poster_path" : posterPath,
      "id"  : id,
      "adult" : adult,
      "backdrop_path"  : backdropPath,
      "original_title" : originalTitle,
      "title" : title,
      "vote_average" : voteAverage,
      "overview"  : overview,
      "release_date" : releaseDate
    };
  }

  Result({
    this.popularity,
    this.voteCount,
    this.video,
    this.posterPath,
    this.id,
    this.adult,
    this.backdropPath,
    this.originalTitle,
    this.title,
    this.voteAverage,
    this.overview,
    this.releaseDate,
  });
}

enum OriginalLanguage { EN, JA, FR, KO }