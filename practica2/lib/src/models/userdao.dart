import 'dart:convert';

class UserDAO {
  String usuario;
  String password;
  String accessToken;

  int id;
  String nomUser;
  String apepUser;
  String apemUser;
  String telUser;
  String mailUser;
  String foto;
  String curp;

  UserDAO({
    this.id,
    this.usuario,
    this.accessToken,
    this.password,
    this.nomUser,
    this.apepUser,
    this.apemUser,
    this.telUser,
    this.mailUser,
    this.foto, 
    this.curp
  });
  
  
  factory UserDAO.fromJSON(Map<String,dynamic> map){
    return UserDAO(
      id       : map['id'],
      nomUser  : map['nomUser'],
      apepUser : map['apepUser'],
      apemUser : map['apemUser'],
      telUser  : map['telUser'],
      mailUser : map['mailUser'],
      foto     : map['foto'],
      curp     : map['curp'],
      usuario : map['usuario'],
      password  : map['password'],
      accessToken  : map['accessToken']
    );
  }

  Map<String,dynamic> toJSON(){
    return {
      "id"       : id,
      "nomUser"  : nomUser,
      "apepUser" : apepUser,
      "apemUser" : apemUser,
      "telUser"  : telUser,
      "mailUser" : mailUser,
      "foto"     : foto,
      "curp"     : curp,
      "usuario" : usuario,
      "password"  : password
    };
  }

  String UserToJSON(){
    final mapUser = this.toJSON();
    return json.encode(mapUser);
  }
}
