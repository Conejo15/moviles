import 'package:flutter/material.dart';
import 'package:practica2/src/assets/preferences.dart';
import 'package:practica2/src/screen/dashboard.dart';
import 'package:practica2/src/screen/detailMovie.dart';
import 'package:practica2/src/screen/favorites.dart';
import 'package:practica2/src/screen/login.dart';
import 'package:practica2/src/screen/profile.dart';
import 'package:practica2/src/screen/search.dart';
import 'package:practica2/src/screen/splashscreen.dart';
import 'package:practica2/src/screen/trending.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  
  Preferences pref = Preferences();
  String data = await pref.getString("token");
  String email = await pref.getString("email");
  
  runApp( MyApp( data != null , email ));
}

class MyApp extends StatefulWidget {
  final bool logueado;
  final String email;
  MyApp(this.logueado, this.email, {Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String email;
  bool logueado;

  @override
  void initState(){
    super.initState();
    email = widget.email;
    logueado =  widget.logueado;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: logueado ? Splashscreen() : Login(),
      routes: {
        '/login' : (BuildContext context) => Login(),
        '/dashboard' : (BuildContext context) => Dashboard( email ),
        '/trending' : (BuildContext context) => Trending(),
        '/profile' : (BuildContext context) => Profile( null,""),
        '/search' : (BuildContext context) => Search(),
        '/favorites' : (BuildContext context) => Favorites(),
        '/detail' : (BuildContext context) => DetailMovie( null, null, null ),
      },
    );
  }
}
